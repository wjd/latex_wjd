TY  - JOUR
AU  - DeMeo, William
PY  - 2013
DA  - 2013/05/01
TI  - Expansions of finite algebras and their congruence lattices
JO  - Algebra universalis
SP  - 257
EP  - 278
VL  - 69
IS  - 3
AB  - In this paper, we present a novel approach to the construction of new finite algebras and describe the congruence lattices of these algebras. Given a finite algebra $${\langle B_0, \ldots \rangle}$$, let $${B_1,B_2, \ldots , B_K}$$be sets that either intersect B0 or intersect each other at certain points. We construct an overalgebra$${\langle A, FA \rangle}$$, by which we mean an expansion of $${\langle B_0, \ldots \rangle}$$with universe $${A = B_0 \cup B_1 \cup \ldots \cup B_K}$$, and a certain set FAof unary operations that includes mappings eisatisfying $${e^2_i = e_i}$$and ei(A) =  Bi, for $${0 \leq i \leq K}$$. We explore two such constructions and prove results about the shape of the new congruence lattices Con$${\langle A, F_A \rangle}$$that result. Thus, descriptions of some new classes of finitely representable lattices is one contribution of this paper. Another, perhaps more significant, contribution is the announcement of a novel approach to the discovery of new classes of representable lattices, the full potential of which we have only begun to explore.
SN  - 1420-8911
UR  - https://doi.org/10.1007/s00012-013-0226-3
DO  - 10.1007/s00012-013-0226-3
ID  - DeMeo2013
ER  - 
