(TeX-add-style-hook
 "cats"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("amsart" "A4")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "twoside" "includehead" "top=\\OPTtopmargin" "bottom=\\OPTbottommargin" "inner=\\OPTinnermargin" "outer=\\OPToutermargin")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "inputs/tikz-cats-01"
    "inputs/tikz-cats-02"
    "inputs/tikz-cats-03"
    "inputs/TikzCatOne"
    "inputs/TikzCatTwo"
    "inputs/TikzCatThree"
    "inputs/TikzCompositionOp"
    "inputs/TikzGraphLoops"
    "inputs/TikzGraphRep"
    "inputs/TikzProductPQ"
    "inputs/TikzProductQP"
    "inputs/TikzProductPQP"
    "inputs/TikzCoproduct"
    "inputs/TikzDisjointUnion"
    "inputs/TikzProductOfMorphisms"
    "inputs/TikzExponential"
    "inputs/TikzGraphExponential"
    "inputs/TikzGraphExpEval"
    "inputs/TikzGraphExpLambda"
    "inputs/TikzFunctor"
    "inputs/TikzFunctorKleene"
    "inputs/TikzFunctorU"
    "inputs/TikzRepresentableFunctor"
    "inputs/TikzRepresentableFunctor2"
    "inputs/TikzRepresentableFunctor3"
    "inputs/TikzRepresentableFunctorContra"
    "amsart"
    "amsart10"
    "amssymb"
    "tikz"
    "url"
    "geometry"
    "hyperref")
   (TeX-add-symbols
    '("drawgrid" 4)
    "figscale"
    "OPTtopmargin"
    "OPTbottommargin"
    "OPTinnermargin"
    "OPTbindingoffset"
    "OPToutermargin")
   (LaTeX-add-environments
    '("Highlighting" LaTeX-env-args ["argument"] 0)))
 :latex)

