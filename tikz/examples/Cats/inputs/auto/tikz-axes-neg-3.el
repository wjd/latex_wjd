(TeX-add-style-hook
 "tikz-axes-neg-3"
 (lambda ()
   (LaTeX-add-environments
    '("Highlighting" LaTeX-env-args ["argument"] 0)
    '("claimproof" LaTeX-env-args ["argument"] 0)))
 :latex)

