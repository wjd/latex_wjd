(TeX-add-style-hook
 "tikz-simple-comm"
 (lambda ()
   (LaTeX-add-environments
    '("Highlighting" LaTeX-env-args ["argument"] 0)
    '("claimproof" LaTeX-env-args ["argument"] 0)))
 :latex)

